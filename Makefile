# make
# Creates dynamic library for vector and builds

# make ARCHIVE_TYPE=static
# Creates static library for vector and builds

SRC_FILES = test_file.c
O_FILES = ${patsubst %.c, %.o, $(SRC_FILES)}
H_FILES = 

C_FLAGS = -I . -I $(ARCHIVE_PATH)
LIB_FLAGS = -L $(ARCHIVE_PATH)
ADD_FLAGS = -Wall -std=c11
MACROS = -D DEBUG

ARCHIVE_TYPE = shared
ARCHIVE_PATH = vector_lib
BIN_FILE = test
BIN_FILE_DEPENDENCIES = $(O_FILES)

R_PATH =

ifeq ($(ARCHIVE_TYPE), shared)
	R_PATH += -Wl,-rpath=$(ARCHIVE_PATH)
else ifeq ($(ARCHIVE_TYPE), static)
	BIN_FILE_DEPENDENCIES += $(ARCHIVE_PATH)/$(ARCHIVE_NAME)
else
	exit
endif


.PHONY: lib coverage

all: lib $(BIN_FILE)

lib:
	make -C $(ARCHIVE_PATH) ARCHIVE_TYPE=$(ARCHIVE_TYPE) MACROS="$(MACROS)"

$(BIN_FILE): $(BIN_FILE_DEPENDENCIES)
	gcc --coverage $< -o $@ -lvector $(LIB_FLAGS) $(ADD_FLAGS) $(R_PATH)

$(O_FILES): $(SRC_FILES) $(H_FILES)
	gcc --coverage -c $^ $(C_FLAGS) $(ADD_FLAGS) $(MACROS)

report:
	make report -C $(ARCHIVE_PATH)
	gcov -abcfu $(SRC_FILES)

.PHONY: clean

clean:
	make clean -C $(ARCHIVE_PATH)
	rm -f $(O_FILES)
	rm -f $(BIN_FILE)
	rm -f ${patsubst %.c, %.gcno, $(SRC_FILES)}
	rm -f ${patsubst %.c, %.gcda, $(SRC_FILES)}
	rm -f *.c.gcov
