#ifndef __VECTOR_H__
#define __VECTOR_H__

#define TYPE_NAME_LEN 8
extern const char vector_element_type[][TYPE_NAME_LEN];

#define SUCCESS 0
#define FAILURE -1

#define THROW(err) throw_error(err, __FILE__, __func__, __LINE__)

/* Enable DEBUG macro to display error messages */
#ifdef DEBUG
#define PROPAGATE_ERR(err) do {	\
		{ THROW(err); return err; }	\
	} while (0)
#else
#define PROPAGATE_ERR(err) return err
#endif

enum err_code {
	EMPTY_VECTOR = -10,
	POS_INVALID = -11,
	SPLICE_POS_INVALID = -12,
	NULL_PTR = -20,
	NO_MEM_ALLOC = -21,
	TYPE_INVALID = -30
};

#define MSG_EMPTY_VECTOR "Vector is empty"
#define MSG_POS_INVALID "Position not exists in vector"
#define MSG_SPLICE_POS_INVALID "Vector cannot be spliced at given position"
#define MSG_NULL_PTR "Encountered unexpected null pointer"
#define MSG_NO_MEM_ALLOC "Dynamic memory not allocated"
#define MSG_TYPE_INVALID "Undefined field type"

void throw_error(enum err_code, const char*, const char*, int);

union g_datatype {
	char char_g;
	short short_g;
	int int_g;
	long long_g;
	float float_g;
	double double_g;
};

enum g_typename {
	CHAR = 1,
	SHORT,
	INT,
	LONG,
	FLOAT,
	DOUBLE
};

#define ENUM_TYPE_MIN CHAR
#define ENUM_TYPE_MAX DOUBLE

struct element {
	union g_datatype field;
	enum g_typename field_type;
};

struct vector {
	struct vector_attribs *va;

	int (*append)(struct vector*, struct element);
	int (*prepend)(struct vector*, struct element);
	int (*insert)(struct vector*, int, struct element);

	int (*chop)(struct vector *);
	int (*behead)(struct vector *);
	int (*delete)(struct vector *, int);

	int (*set)(struct vector*, int, struct element);

	int (*get)(struct vector*, int, struct element*);

	int (*is_empty)(struct vector *);

	int (*first)(struct vector*, struct element*);
	int (*last)(struct vector*, struct element*);
	int (*clear)(struct vector *);
	int (*destruct)(struct vector *);
	int (*size)(struct vector *);
	int (*move)(struct vector*, int, int);
	struct vector *(*splice)(struct vector*, int);

	int (*display)(struct vector *);
};

#define ELEMENT(data, type) ((struct element) \
	{ .field = (union g_datatype)data, .field_type = type})

#define VALUE(entry)  entry->element
#define TYPE(entry)   entry->element.field_type

#define g_println(element) do {	\
		g_print(element);	\
		printf("\n");	\
	} while (0)
#define g_print(element) do {	\
		switch ((element).field_type) {	\
		case CHAR:	\
		printf("%c", (element).field.char_g); break;	\
		case SHORT:	\
		printf("%hd", (element).field.short_g); break;	\
		case INT:	\
		printf("%d", (element).field.int_g); break;	\
		case LONG:	\
		printf("%ld", (element).field.long_g); break;	\
		case FLOAT:	\
		printf("%g", (element).field.float_g); break;	\
		case DOUBLE:	\
		printf("%lg", (element).field.double_g); break;	\
		}	\
		printf(" - %s\t",	\
			vector_element_type[(element).field_type]);	\
	} while (0)

/* # Public APIs # */
struct vector vector(struct vector *vector);

#if 0
/* # Internal APIs # */
int append(struct vector *vector, struct element element);
int prepend(struct vector *vector, struct element element);
int insert(struct vector *vector, int pos, struct element element);

int chop(struct vector *vector);
int behead(struct vector *vector);
int delete(struct vector *vector, int pos);

/* Removes all the elements, but vector is available for other operations */
int clear(struct vector *vector);

/* Frees the complete vector useful only when dynamically allocated */
int destruct(struct vector *vector);

int move(struct vector *vector, int old_pos, int new_pos);

int set(struct vector *vector, int pos, struct element element);

/* Gives the element value but doesn't alter the list */
int first(struct vector *vector, struct element *element);
int last(struct vector *vector, struct element *element);
int get(struct vector *vector, int pos, struct element *element);

int is_empty(struct vector *vector);
int size(struct vector *vector);

/* Splices the vector into two and returns second vector
 * which starts from the `pos`.
 */
struct vector *splice(struct vector *vect, int pos);

/* Displays the contents of vector in both forward and reverse traversals */
int display(struct vector *);
#endif


#endif /* __VECTOR_H__ */

