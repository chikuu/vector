#include <stdio.h>
#include <stdlib.h>
#include <vector.h>

#define for_each(entry, vector)	\
	for (struct vector_node *entry = (vector).va->head;	\
		entry != NULL; entry = entry->next)

#define for_each_rev(entry, vector)                  \
	for (struct vector_node *entry = (vector).va->tail; \
		entry != NULL; entry = entry->prev)

#define IN_RANGE(x, a, b) (((x) >= (a)) && ((x) <= (b)))

static int append(struct vector*, struct element);
static int prepend(struct vector*, struct element);
static int insert(struct vector*, int, struct element);
static int chop(struct vector*);
static int behead(struct vector*);
static int delete(struct vector*, int);
static int clear(struct vector*);
static int destruct(struct vector*);
static int move(struct vector*, int, int);
static int set(struct vector*, int, struct element);
static int first(struct vector*, struct element*);
static int last(struct vector*, struct element*);
static int get(struct vector*, int, struct element*);
static int is_empty(struct vector*);
static int size(struct vector*);
static struct vector *splice(struct vector*, int);
static int display(struct vector *);

const char vector_element_type[][TYPE_NAME_LEN] = {
	[CHAR] = "char",
	[SHORT] = "short",
	[INT] = "int",
	[LONG] = "long",
	[FLOAT] = "float",
	[DOUBLE] = "double"
};

struct vector_node {
	struct element element;
	struct vector_node *next;
	struct vector_node *prev;       
};

struct vector_attribs {
	struct vector_node *head;
	struct vector_node *tail;
	int node_count;
};

void throw_error(enum err_code err, const char *file, const char *func, int line)
{
	switch (err) {
		case NO_MEM_ALLOC:
		printf(MSG_NO_MEM_ALLOC); break;
		case POS_INVALID:
		printf(MSG_POS_INVALID); break;
		case NULL_PTR:
		printf(MSG_NULL_PTR); break;
		case EMPTY_VECTOR:
		printf(MSG_EMPTY_VECTOR); break;
		case SPLICE_POS_INVALID:
		printf(MSG_SPLICE_POS_INVALID); break;
		case TYPE_INVALID:
		printf(MSG_TYPE_INVALID); break;
		default:
		printf("Unknown error");
	}

#ifdef DEBUG
	printf(" - %s - ", file);
	printf("%s() - ", func);
	printf("in line %d", line);
#endif
	printf("\n");
}

static int get_node(struct vector *vector, int pos, struct vector_node **node)
{
	if (vector == NULL)
		PROPAGATE_ERR(NULL_PTR);

	if (is_empty(vector))
		PROPAGATE_ERR(EMPTY_VECTOR);

	if (pos < 0 || pos >= vector->va->node_count)
		PROPAGATE_ERR(POS_INVALID);

	if (vector->va->node_count / 2 >= pos) {
		*node = vector->va->head;
		while (pos--)
			*node = (*node)->next;
	} else {
		*node = vector->va->tail;
		pos = vector->va->node_count - pos;
		while (--pos)
			*node = (*node)->prev;
	}

	return SUCCESS;
}

static int place_node(struct vector *vector, int pos, struct vector_node *node)
{
	if (vector == NULL || node == NULL)
		PROPAGATE_ERR(NULL_PTR);;

	if (pos < 0 || pos > vector->va->node_count)
		PROPAGATE_ERR(POS_INVALID);;

	node->prev = node->next = NULL;

	if (vector->va->node_count == 0) {
		vector->va->head = vector->va->tail = node;
		return SUCCESS;
	}

	struct vector_node *left = vector->va->head;
	struct vector_node *right = vector->va->tail;
	
	if (pos == 0) {
		node->next = left;
		left->prev = node;
		vector->va->head = node;
	} else if (pos == vector->va->node_count) {
		node->prev = right;
		right->next = node;
		vector->va->tail = node;
	} else {
		if (vector->va->node_count / 2 >= pos) {
			while (--pos)
				left = left->next;
			right = left->next;	
		} else {
			pos = vector->va->node_count - pos;
			while (--pos)
				right = right->prev;
			left = right->prev;
		}
		node->next = right;
		right->prev = node;
		node->prev = left;
		left->next = node;
	}

	return SUCCESS;
}

static void reset(struct vector *vector)
{
	vector->va->head = NULL;
	vector->va->tail = NULL;
	vector->va->node_count = 0;
}

struct vector vector(struct vector *vector)
{
	struct vector v;

	if (vector == NULL)
		vector = &v;

	vector->va = malloc(sizeof(struct vector_attribs));
	reset(vector);

	vector->append = append;
	vector->prepend = prepend;
	vector->insert = insert;
	vector->chop = chop;
	vector->behead = behead;
	vector->delete = delete;
	vector->set = set;
	vector->get = get;
	vector->is_empty = is_empty;
	vector->first = first;
	vector->last = last;
	vector->clear = clear;
	vector->destruct = destruct;
	vector->size = size;
	vector->move = move;
	vector->splice = splice;
	vector->display = display;

	return *vector;
}

static inline int append(struct vector *vector, struct element element)
{
	return insert(vector, vector->va->node_count, element);
}

static inline int prepend(struct vector *vector, struct element element)
{
	return insert(vector, 0, element);
}

static int insert(struct vector *vector, int pos, struct element element)
{
	int status;

	if (vector == NULL)
		PROPAGATE_ERR(NULL_PTR);

	if (pos < 0 || pos > vector->va->node_count)
		PROPAGATE_ERR(POS_INVALID);

	if (!IN_RANGE(element.field_type, ENUM_TYPE_MIN, ENUM_TYPE_MAX))
		PROPAGATE_ERR(TYPE_INVALID);

	struct vector_node *node;
	if (!(node = malloc(sizeof(struct vector_node))))
		PROPAGATE_ERR(NO_MEM_ALLOC);

	if ((status = place_node(vector, pos, node)) != SUCCESS) {
		free(node);
		PROPAGATE_ERR(status);
	}
	
	node->element = element;
	++vector->va->node_count;

	return SUCCESS;
}

static inline int chop(struct vector *vector)
{
	return delete(vector, vector->va->node_count - 1);
}

static inline int behead(struct vector *vector)
{
	return delete(vector, 0);
}

static int delete(struct vector *vector, int pos)
{
	struct vector_node *target;
	int status;

	if ((status = get_node(vector, pos, &target)) != SUCCESS)
		PROPAGATE_ERR(status);

	if (vector->va->node_count == 1) {
		free(target);
		target = NULL;
		reset(vector);
		return SUCCESS;
	}

	if (pos == 0) {
		vector->va->head = target->next;
		vector->va->head->prev = NULL;
	} else if (pos == vector->va->node_count - 1) {
		vector->va->tail = target->prev;
		vector->va->tail->next = NULL;
	} else {
		target->prev->next = target->next;
		target->next->prev = target->prev;
	}

	free(target);
	target = NULL;
	--vector->va->node_count;

	return SUCCESS;
}

static int set(struct vector *vector, int pos, struct element element)
{
	struct vector_node *target;
	int status;

	if ((status = get_node(vector, pos, &target)) != SUCCESS)
		PROPAGATE_ERR(status);

	if (!IN_RANGE(element.field_type, ENUM_TYPE_MIN, ENUM_TYPE_MAX))
		PROPAGATE_ERR(TYPE_INVALID);

	target->element = element;

	return SUCCESS;
}

static int get(struct vector *vector, int pos, struct element *element)
{
	struct vector_node *target;
	int status;

	if ((status = get_node(vector, pos, &target)) != SUCCESS)
		PROPAGATE_ERR(status);

	*element = target->element;

	return SUCCESS;
}

static inline int first(struct vector *vector, struct element *element)
{
	return get(vector, 0, element);
}

static inline int last(struct vector *vector, struct element *element)
{
	return get(vector, vector->va->node_count - 1, element);
}

static inline int is_empty(struct vector *vector)
{
	if (vector == NULL)
		PROPAGATE_ERR(NULL_PTR);

	return vector->va->node_count == 0;
}

static inline int size(struct vector *vector)
{
	if (vector == NULL)
		PROPAGATE_ERR(NULL_PTR);

	return vector->va->node_count;
}

static int clear(struct vector *vector)
{
	int status;

	while (vector->va->node_count)
		if ((status = delete(vector, 0)) != SUCCESS)
			PROPAGATE_ERR(status);

	return SUCCESS;
}

static int destruct(struct vector *vector)
{
	int status;

	if ((status = clear(vector)) != SUCCESS)
		PROPAGATE_ERR(status);

	free(vector->va);
	free(vector);

	return SUCCESS;
}

static int move(struct vector *vector, int old_pos, int new_pos)
{
	int status;
	struct vector_node *target;

	if (new_pos < 0 || new_pos > vector->va->node_count)
		PROPAGATE_ERR(POS_INVALID);

	if (old_pos == new_pos)
		return SUCCESS;

	if ((status = get_node(vector, old_pos, &target)) != SUCCESS)
		PROPAGATE_ERR(status);

	if (old_pos == 0) {
		vector->va->head = target->next;
		vector->va->head->prev = NULL;
	} else if (old_pos == vector->va->node_count - 1) {
		vector->va->tail = target->prev;
		vector->va->tail->next = NULL;
	} else {
		target->prev->next = target->next;
		target->next->prev = target->prev;
	}

	if (old_pos < new_pos)
		++new_pos;

	if ((status = place_node(vector, new_pos, target)) != SUCCESS) 
		PROPAGATE_ERR(status);

	return SUCCESS;
}

static struct vector *splice(struct vector *vect, int pos)
{
	int status;

	if (pos == 0 || pos == vect->va->node_count - 1) {
		THROW(SPLICE_POS_INVALID);
		return NULL;
	}

	struct vector_node *new_vect_head;
	struct vector_node *new_vect_tail;

	if (
		((status = get_node(vect, pos, &new_vect_head)) != SUCCESS) ||
		((status = get_node(vect, vect->va->node_count-1, &new_vect_tail))
		!= SUCCESS)
	) {
		THROW(status);
		return NULL;
	}

	struct vector *new_vector = NULL;
	*new_vector = vector(NULL);

	vect->va->tail = new_vect_head->prev;
	vect->va->tail->next = NULL;

	new_vect_head->prev = NULL;
	new_vector->va->head = new_vect_head;
	new_vector->va->tail = new_vect_tail;

	new_vector->va->node_count = vect->va->node_count - pos;
	vect->va->node_count = pos;

	return new_vector;

}

static int display(struct vector *vector)
{
	int status;

	status = is_empty(vector);
	if (status == 0)
		;
	else if (status == 1)
		PROPAGATE_ERR(EMPTY_VECTOR);
	else
		PROPAGATE_ERR(status);

	printf("\n\n");
	for_each(entry, *vector)
		g_print(VALUE(entry));
	printf("\n-------------------------\n");
	for_each_rev(entry, *vector)
		g_print(VALUE(entry));
	printf("\n\n");

	return SUCCESS;
}
