#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <vector.h>

#define IN_RANGE(x, a, b) (((x) >= (a)) && ((x) <= (b)))

#define RANDOM_VALUES
#define MAX_RANGE 10000

#define DECIMAL_BASE 10

#define MSG_INVALID_DATA "Invalid data. Re-enter\n\n"
#define MSG_UNKNOWN_ERROR "Unknown error occured\n\n"

#define READ_INT(x) if (read_int(&x) == FAILURE) {	\
	printf(MSG_INVALID_DATA);	\
	break;	\
}

#define READ_ELEMENT(x) if (read_element(&x) == FAILURE) {	\
	printf(MSG_INVALID_DATA);	\
	break;	\
}

static void read_string(char *string, int buffer_size)
{
	int length;
	char *retp;

	do
		retp = fgets(string, buffer_size + 1, stdin);
	while (retp == NULL || *retp == '\n');

	length = strlen(retp);
	if (retp[length - 1] == '\n')
		retp[length - 1] = '\0';
	else
		while (getchar() != '\n')
			;
}

static int read_int(void *buf)
{
	char read_buf[10];
	char *end_ptr;

	read_string(read_buf, sizeof(read_buf));
	*(long *)buf = strtol(read_buf, &end_ptr, DECIMAL_BASE);

	if (!IN_RANGE(*(long *)buf, INT_MIN, INT_MAX) ||
		*end_ptr != '\0')
		return FAILURE;

	return SUCCESS;
}

static int read_element(struct element *e)
{
	long val;

#ifdef RANDOM_VALUES
	val = rand() % MAX_RANGE;
#else
	if (read_int(&val) == FAILURE)
		return FAILURE;
#endif
	*e = ELEMENT((int)val, INT);

	return SUCCESS;
}

static int bubble_sort(struct vector *vector)
{
	int i;
	int j;
	int status;
	int size = vector->size(vector);
	struct element left;
	struct element right;

	for (i = size - 1; i > 0; --i)
		for (j = 0; j < i; ++j) {
			if (vector->get(vector, j, &left) != SUCCESS ||
				vector->get(vector, j + 1, &right) != SUCCESS)
				return -1;
			if (left.field.int_g > right.field.int_g) {
				status = vector->move(vector, j, j+1);
				if (status != SUCCESS)
					return -2;
			}
		}

	return 0;
}

static void simple_print(struct vector *vector)
{
	int i;
	int size = vector->size(vector);
	struct element element;

	for (i = 0; i < size; ++i) {
		vector->get(vector, i, &element);
		printf("%d\t", element.field.int_g);
	}

	printf("\n");
}

int main(void)
{
	int n;
	int i;
	int status;
	struct element data;
	struct vector vect = vector(NULL);
	clock_t start;
	double store_period;
	double sort_period;

	srand(time(NULL));

	printf("Enter no. of numeric elements\t");
	do {
		if (read_int(&n) == FAILURE) {
			printf(MSG_INVALID_DATA);
			continue;
		}
	} while (0);

	start = clock();
	for (i = 0; i < n; ++i) {
#ifndef RANDOM_VALUES
		printf("Enter data %d\t", i + 1);
#endif
		if (read_element(&data) != SUCCESS ||
			vect.append(&vect, data) != SUCCESS) {
			printf(MSG_INVALID_DATA);
			--i;
			continue;
		}
	}
	store_period = (double)(clock() - start) / CLOCKS_PER_SEC * 1000;
	if (i != n) {
		printf(MSG_UNKNOWN_ERROR);
	} else {
		start = clock();
		status = bubble_sort(&vect);
		sort_period = (double)(clock() - start) / CLOCKS_PER_SEC * 1000;
		if (status == SUCCESS) {
			printf("Vector after sorting\n");
			simple_print(&vect);
			printf("Vector completed in %lgms\n", store_period);
			printf("Sort completed in %lgms\n", sort_period);
		} else {
			printf(MSG_UNKNOWN_ERROR);
		}
	}

	vect.clear(&vect);

	return 0;
}
